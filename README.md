# Hilbert in your browser

An example program to generate and display SVG directly in the browser using the Haskell [diagrams library](https://hackage.haskell.org/package/diagrams).

The code was originally taken from:

> [Haskell art in your browser with Asterius](https://www.tweag.io/blog/2019-12-19-asterius-diagrams/)  
> _19 December 2019 — by Sylvain Henry (IOHK), Cheng Shao_
